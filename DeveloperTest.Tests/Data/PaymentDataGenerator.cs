﻿using DeveloperTest.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeveloperTest.Tests
{
    public static class PaymentDataGenerator
    {
        public struct PaymentData
        {
            public MakePaymentRequest payRequest;
            public Account creditor;
            public Account debtor;
        }

        /// <summary>
        /// Generates a known good preset for data
        /// </summary>
        /// <returns></returns>
        public static PaymentData Generate()
        {
            PaymentData data;
            data.payRequest = new MakePaymentRequest
            {
                Amount = 1,
                CreditorAccountNumber = "2",
                DebtorAccountNumber = "3",
                PaymentDate = DateTime.Now.Date,
                PaymentScheme = PaymentScheme.Bacs
            };
            data.creditor = new Account
            {
                AccountNumber = "2",
                AllowedPaymentSchemes = AllowedPaymentSchemes.Bacs,
                Balance = 20,
                Status = AccountStatus.Live
            };
            data.debtor = new Account
            {
                AccountNumber = "3",
                AllowedPaymentSchemes = AllowedPaymentSchemes.Bacs,
                Status = AccountStatus.Live,
                Balance = 12
            };
            return data;
        }
    }
}
