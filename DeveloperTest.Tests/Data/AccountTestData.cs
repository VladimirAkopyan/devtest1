﻿using DeveloperTest;
using DeveloperTest.Types;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace DeveloperTest.Tests
{
    public class AccountTestData : TheoryData<MakePaymentRequest, Account, Account, bool>
    {
        public AccountTestData()
        {
            var data = PaymentDataGenerator.Generate(); 

            Add(data.payRequest, data.creditor, data.debtor, true);
            Add(data.payRequest, null, data.debtor, false);
            Add(data.payRequest, data.creditor, null, false);
            Add(data.payRequest, null, null, false);
        }
    }
}
