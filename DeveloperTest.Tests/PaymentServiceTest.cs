using DeveloperTest.Data;
using DeveloperTest.Types;
using DeveloperTest.Services;
using Moq;
using System;
using Xunit;

namespace DeveloperTest.Tests
{
    public class PaymentServiceTest
    {
        /// <summary>
        /// 9 cases, 3 minimal true flags, 3 false conditions with all but one required flag set for creditor, then same for debtor
        /// </summary>
        [Theory]
        [InlineData(AllowedPaymentSchemes.Bacs, AllowedPaymentSchemes.Bacs, PaymentScheme.Bacs, true)]
        [InlineData(AllowedPaymentSchemes.Chaps, AllowedPaymentSchemes.Chaps, PaymentScheme.Chaps, true)]
        [InlineData(AllowedPaymentSchemes.FasterPayments, AllowedPaymentSchemes.FasterPayments, PaymentScheme.FasterPayments, true)]
        [InlineData(int.MaxValue, AllowedPaymentSchemes.Chaps & AllowedPaymentSchemes.Bacs, PaymentScheme.FasterPayments, false)]
        [InlineData(int.MaxValue, AllowedPaymentSchemes.FasterPayments & AllowedPaymentSchemes.Bacs, PaymentScheme.Chaps, false)]
        [InlineData(int.MaxValue, AllowedPaymentSchemes.FasterPayments & AllowedPaymentSchemes.Chaps, PaymentScheme.Bacs, false)]
        [InlineData(AllowedPaymentSchemes.Chaps & AllowedPaymentSchemes.Bacs, int.MaxValue, PaymentScheme.FasterPayments, false)]
        [InlineData(AllowedPaymentSchemes.FasterPayments & AllowedPaymentSchemes.Bacs, int.MaxValue, PaymentScheme.Chaps, false)]
        [InlineData(AllowedPaymentSchemes.FasterPayments & AllowedPaymentSchemes.Chaps, int.MaxValue, PaymentScheme.Bacs, false)]
        public void PaymentSchemes(AllowedPaymentSchemes debtorSchemes, AllowedPaymentSchemes creditorSchemes, 
            PaymentScheme paymentScheme, bool shouldSucceed)
        {
            var data = PaymentDataGenerator.Generate();
            data.creditor.AllowedPaymentSchemes = creditorSchemes;
            data.debtor.AllowedPaymentSchemes = debtorSchemes;
            data.payRequest.PaymentScheme = paymentScheme;
            var moqDb = MockDataStore(data.payRequest, data.creditor, data.debtor);
            var paymentService = new PaymentService(moqDb.Object);
            var result = paymentService.MakePayment(data.payRequest);
            Assert.True(result.Success == shouldSucceed);
        }

        /// <summary>
        /// 4 cases: excessive balance, exact balalnce, insufficient balance, negative balance. 
        /// Checks that correct balance is produced and that the changes were saved. 
        /// </summary>
        [Theory]
        [InlineData(10, 0, 5, true)]
        [InlineData(0.1, 0, 0.1, true)]
        [InlineData(5, 5, 10, false)]
        [InlineData(-10, -5, 10, false)]
        public void AccountBalance(decimal debtorBalance, decimal creditorBalance, decimal paymentAmount, bool ShouldSucceed)
        {
            var data = PaymentDataGenerator.Generate();
            data.payRequest.Amount = paymentAmount;
            data.creditor.Balance = creditorBalance;
            data.debtor.Balance = debtorBalance; 
            var moqDb = MockDataStore(data.payRequest, data.creditor, data.debtor);
            var paymentService = new PaymentService(moqDb.Object);
            var result = paymentService.MakePayment(data.payRequest);
            Assert.True(result.Success == ShouldSucceed);

            if (ShouldSucceed)
            {
                Assert.True(data.debtor.Balance == (debtorBalance - paymentAmount));
                Assert.True(data.creditor.Balance == (creditorBalance + paymentAmount));
                moqDb.Verify(db => db.UpdateAccount(data.creditor), Times.Once);
                moqDb.Verify(db => db.UpdateAccount(data.debtor), Times.Once);
            }
            else
            {
                Assert.True(data.debtor.Balance == debtorBalance);
                Assert.True(data.creditor.Balance == creditorBalance);
                moqDb.Verify(db => db.UpdateAccount(data.creditor), Times.Never);
                moqDb.Verify(db => db.UpdateAccount(data.debtor), Times.Never);
            }

        }

        /// <summary>
        /// 3 cases - both present, one missing, second missing, both missing
        /// </summary>
        [Theory]
        [InlineData(0, true)]
        [InlineData(+24, false)]
        [InlineData(-24, false)]
        [InlineData(+1, false)]
        public void PaymentDateTest(int extraHours, bool shouldSucceed)
        {
            var data = PaymentDataGenerator.Generate();
            data.payRequest.PaymentDate = DateTime.Now.Date.AddHours(extraHours); 
            var datastore = MockDataStore(data.payRequest, data.creditor, data.debtor);
            var paymentService = new PaymentService(datastore.Object);
            var result = paymentService.MakePayment(data.payRequest);
            Assert.True(result.Success == shouldSucceed);
        }

        /// <summary>
        /// 3 cases - both present, one missing, second missing, both missing
        /// </summary>
        [Theory]
        [ClassData(typeof(AccountTestData))]
        public void AccountsMissingTest(MakePaymentRequest request, Account creditor, Account debtor, bool shouldSucceed)
        {
            var datastore = MockDataStore(request, creditor, debtor);
            var paymentService = new PaymentService(datastore.Object);
            var result = paymentService.MakePayment(request); 
            Assert.True(result.Success == shouldSucceed);
        }

        [Theory]
        [InlineData(AccountStatus.Live,  AccountStatus.Live, true)]
        [InlineData(AccountStatus.Live, AccountStatus.InboundPaymentsOnly, true)]
        [InlineData(AccountStatus.Live, AccountStatus.Disabled, false)]
        [InlineData(AccountStatus.InboundPaymentsOnly, AccountStatus.Live, false)]
        [InlineData(AccountStatus.Disabled, AccountStatus.Live, false)]
        public void AccountStatusTest(AccountStatus debtorStatus, AccountStatus creditorStatus, bool ShouldSucceed)
        {
            var data = PaymentDataGenerator.Generate();
            data.creditor.Status = creditorStatus;
            data.debtor.Status = debtorStatus; 
            var moqDB = MockDataStore(data.payRequest, data.creditor, data.debtor);
            var paymentService = new PaymentService(moqDB.Object);
            var result = paymentService.MakePayment(data.payRequest);
            Assert.True(result.Success == ShouldSucceed);
        }

        Mock<IAccountDatastore> MockDataStore(MakePaymentRequest request, Account creditor = null, Account debtor = null)
        {
            var moqDb = new Mock<IAccountDatastore>();
            moqDb.Setup(db => db.GetAccount(It.Is<string>(s => s == request.CreditorAccountNumber))).Returns(creditor);
            moqDb.Setup(db => db.GetAccount(It.Is<string>(s => s == request.DebtorAccountNumber))).Returns(debtor);
            moqDb.Setup(db => db.GetAccount(It.Is<string>(s =>      //Moq does not accept "normal" null
            s != request.DebtorAccountNumber && s != request.CreditorAccountNumber))).Returns((Account)null);
            return moqDb; 
        }
    }
}
