﻿using DeveloperTest.Data;
using DeveloperTest.Types;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace DeveloperTest.Services
{
    public class PaymentService : IPaymentService
    {
        IAccountDatastore _DataStore;

        /// <summary>
        /// Used to match payment schemes to corresponding flags
        /// </summary>
        IReadOnlyDictionary<PaymentScheme, AllowedPaymentSchemes> PaymentFlags = new Dictionary<PaymentScheme, AllowedPaymentSchemes>()
        {
            { PaymentScheme.Bacs, AllowedPaymentSchemes.Bacs},
            { PaymentScheme.Chaps, AllowedPaymentSchemes.Chaps},
            { PaymentScheme.FasterPayments, AllowedPaymentSchemes.FasterPayments}
        };

        public PaymentService(IAccountDatastore dataStore)
        {
            _DataStore = dataStore;
        }

        public MakePaymentResult MakePayment(MakePaymentRequest request)
        {
            var result = new MakePaymentResult() { Success = true };
            Account debtorAccount = _DataStore.GetAccount(request.DebtorAccountNumber);
            Account creditorAccount = _DataStore.GetAccount(request.CreditorAccountNumber);
            if( debtorAccount == null || creditorAccount == null) {
                result.Success = false;
                return result; 
            }
            //Logical AND EQUALS check against fault conditions, return if failed. Saves on If statements
            result.Success &= debtorAccount.AllowedPaymentSchemes.HasFlag(PaymentFlags[request.PaymentScheme]);
            result.Success &= creditorAccount.AllowedPaymentSchemes.HasFlag(PaymentFlags[request.PaymentScheme]);
            result.Success &= debtorAccount.Status == AccountStatus.Live;
            result.Success &= creditorAccount.Status != AccountStatus.Disabled;
            result.Success &= debtorAccount.Balance >= request.Amount;
            result.Success &= request.PaymentDate == DateTime.Now.Date;  
            if (result.Success == false)
                return result; 

            debtorAccount.Balance -= request.Amount;
            creditorAccount.Balance += request.Amount; 
            _DataStore.UpdateAccount(debtorAccount);
            _DataStore.UpdateAccount(creditorAccount);             
            return result;
        }
    }
}
