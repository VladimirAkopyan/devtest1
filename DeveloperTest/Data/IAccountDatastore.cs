﻿using DeveloperTest.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeveloperTest.Data
{
    public interface IAccountDatastore
    {
        Account GetAccount(string accountNumber);
        void UpdateAccount(Account account);
    }
}
